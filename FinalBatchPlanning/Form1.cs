﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;

namespace FinalBatchPlanning
{
    public partial class Frm_Main : Form
    {
        string Catalog = "";
        string Floor = "";
        string SqlStr = "";
        int LocNo = 0;
        int screenWidth = 0;
        int screenHeight = 0;

        //public static LocationInfo[] Arr_LI;
        public static Label[] Arr_LI;

        public Frm_Main()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            if(Environment.MachineName.ToUpper().Trim() == "TVRM2")
                Floor = "FL2";
            else
                Floor = "FL1";            

            DrawScreen();           
        }

        private int GetLocationNo()
        {
            int Loc_No = 0;
            try
            {
                DBService dbs = new DBService();
                DataTable Result = new DataTable();
                SqlStr = "select count(WLOC) from BPCSFV30.ILML01 where wwhs='FB' and WZONE='" + Floor + "'";
                Result = dbs.executeSelectQueryNoParam(SqlStr);
                Loc_No = int.Parse(Result.Rows[0][0].ToString());
            }
            catch (Exception Ex)
            {
                System.Diagnostics.Debug.Print(DateTime.Now + " : " + "Error - " + Ex.Message);
                Loc_No = 0;
            }

            return Loc_No;
        }

        private void DrawScreen()
        {
            screenWidth = Screen.PrimaryScreen.Bounds.Width;
            screenHeight = Screen.PrimaryScreen.Bounds.Height;
            //int currentDPI = 0;
            //using (Graphics g = this.CreateGraphics())
            //{
            //    currentDPI = (int)g.DpiX;
            //}
            if (screenWidth == 1920 && screenHeight == 1080)
            {
                GBoxLegend.Height = 55;
                GBoxLegend.Font = new Font("Arial", 12, FontStyle.Bold);
                label1.Height = 31;
                label1.Font = new Font("Arial", 14, FontStyle.Bold);
                label2.Height = 31;
                label2.Font = new Font("Arial", 14, FontStyle.Bold);
                label3.Height = 31;
                label3.Font = new Font("Arial", 14, FontStyle.Bold);
                label4.Height = 31;
                label4.Font = new Font("Arial", 14, FontStyle.Bold);
                label5.Height = 31;
                label5.Font = new Font("Arial", 14, FontStyle.Bold);
                label6.Height = 31;
                label6.Font = new Font("Arial", 14, FontStyle.Bold);
                Lbl_UpdateTo.Height = 31;
                Lbl_UpdateTo.Font = new Font("Arial", 14, FontStyle.Bold);
            }
            else //if (screenWidth == 1280 && screenHeight == 720)
            {
                GBoxLegend.Height = 35;
                GBoxLegend.Font = new Font("Arial", 8, FontStyle.Bold);
                label1.Height = 15;
                label1.Width = 150;
                label1.Top = 16;                
                label1.Font = new Font("Arial", 10, FontStyle.Bold);

                label2.Height = 15;
                label2.Width = 150;
                label2.Top = 16;                
                label2.Font = new Font("Arial", 10, FontStyle.Bold);

                label3.Height = 15;
                label3.Width = 150;
                label3.Top = 16;                
                label3.Font = new Font("Arial", 10, FontStyle.Bold);

                label4.Height = 15;
                label4.Width = 150;
                label4.Top = 16;                
                label4.Font = new Font("Arial", 10, FontStyle.Bold);

                label5.Height = 15;
                label5.Width = 150;
                label5.Top = 16;                
                label5.Font = new Font("Arial", 10, FontStyle.Bold);

                label6.Height = 15;
                label6.Width = 150;
                label6.Top = 16;                
                label6.Font = new Font("Arial", 10, FontStyle.Bold);

                Lbl_UpdateTo.Height = 15;
                Lbl_UpdateTo.Width = 150;
                Lbl_UpdateTo.Top = 16;
                Lbl_UpdateTo.Font = new Font("Arial", 10, FontStyle.Bold);

                label6.Left = 100 + Lbl_UpdateTo.Width + 7;
                label5.Left = 100 + Lbl_UpdateTo.Width + label6.Width + 14;
                label4.Left = 100 + Lbl_UpdateTo.Width + label6.Width + label5.Width + 21;
                label3.Left = 100 + Lbl_UpdateTo.Width + label6.Width + label5.Width + label4.Width + 28;
                label2.Left = 100 + Lbl_UpdateTo.Width + label6.Width + label5.Width + label4.Width + label3.Width + 35;
                label1.Left = 100 + Lbl_UpdateTo.Width + label6.Width + label5.Width + label4.Width + label3.Width + label2.Width + 42;
            }

                DateTime DTTmp;
            try
            {   LocNo = GetLocationNo();}
            catch (Exception Ex)
            {   System.Diagnostics.Debug.Print(DateTime.Now + " : " + "Error - " + Ex.Message);}
            PanelMain.Controls.Clear();

            DBService dbs = new DBService();            
            DataTable Result = new DataTable();
            try
            {
                // תאריך משטח לא מאושר 0 זה משבש הצגת המשטחים בסדר של תאריך עולה
                SqlStr = "select WLOC, LNPROD, SUM(LNRCPT+LNADJT-LNISS) ," +
                                "count(LNLOT), case when min(LNEXDT)=0 then '30000101' else case when min(LNEXDT) is null then '30000101' else min(LNEXDT) end end as DT , LNSTT " +
                         "from BPCSFV30.ILML01 left join BPCSFALI.ILNF on WLOC= LNLOC and LNID='LN' " +
                         "where wwhs='FB'  and WZONE='" + Floor + "' " +
                         "group by wloc, LNPROD, LNSTT " +
                         "order by LNPROD, DT asc, WLOC";
                Result = dbs.executeSelectQueryNoParam(SqlStr);
                Arr_LI = new Label[LocNo];
                Catalog = "";
                ToolTip ttip = new ToolTip();
                for (int i = 0; i < LocNo; i++)
                {
                    Arr_LI[i] = new Label();

                    if (screenWidth == 1920 && screenHeight == 1080)
                    {
                        Arr_LI[i].Size = new Size(400, 35);
                        Arr_LI[i].Font = new Font("Arial", 18, FontStyle.Bold);
                    }
                    else
                    {
                        Arr_LI[i].Size = new Size(250, 20);
                        Arr_LI[i].Font = new Font("Arial", 12, FontStyle.Bold);
                    }

                        Arr_LI[i].TextAlign = ContentAlignment.MiddleCenter;
                    ttip.SetToolTip(Arr_LI[i], "תאריך תפוגה: " + Convert.ToDateTime(FixDT(Result.Rows[i][4].ToString().Trim())).ToString("dd/MM/yyyy"));
                    if (i == 0 || i == 20 || i == 40 || i == 60)
                        LabelHeader(i);
                    if (i < 20)
                    {
                        if (screenWidth == 1920 && screenHeight == 1080)
                            Arr_LI[i].Location = new Point(110, i * 40 + 60);
                        else 
                            Arr_LI[i].Location = new Point(110, i * 24 + 40);
                    }
                    else if (i < 40)
                        if (screenWidth == 1920 && screenHeight == 1080)
                            Arr_LI[i].Location = new Point(540, (i - 20) * 40 + 60);
                        else 
                            Arr_LI[i].Location = new Point(390, (i - 20) * 24 + 40);
                    else if (i < 60)
                        if (screenWidth == 1920 && screenHeight == 1080)
                            Arr_LI[i].Location = new Point(970, (i - 40) * 40 + 60);
                        else 
                            Arr_LI[i].Location = new Point(670, (i - 40) * 24 + 40);
                    else if (i < 80)
                        if (screenWidth == 1920 && screenHeight == 1080)
                            Arr_LI[i].Location = new Point(1400, (i - 60) * 40 + 60);
                        else 
                            Arr_LI[i].Location = new Point(950, (i - 60) * 24 + 40);
                    if (Result.Rows[i][1].ToString().Trim() == "")
                        Arr_LI[i].Text = "                           " + Result.Rows[i][0].ToString() + "            " + Result.Rows[i][3].ToString();
                    else
                        Arr_LI[i].Text = Result.Rows[i][1].ToString() + "            " + Result.Rows[i][0].ToString() + "            " + Result.Rows[i][3].ToString();

                    if (Result.Rows[i][5].ToString().Trim() == "W")
                    {
                        Arr_LI[i].BackColor = Color.DarkTurquoise;
                        ttip.SetToolTip(Arr_LI[i], "תערובת לא מאושרת");
                    }
                    else if (Result.Rows[i][4].ToString().Trim() != "30000101" && Result.Rows[i][4].ToString().Trim() != "")
                    {
                        DTTmp = Convert.ToDateTime(FixDT(Result.Rows[i][4].ToString().Trim()));


                        if (DateTime.Now.Date == DTTmp)
                            Arr_LI[i].BackColor = Color.Coral; // אם היום פג תוקף דינו כדין 3 ימים לפג תוקף - בהנחיית דויד מרקוביץ
                        else if (DateTime.Now.Date > DTTmp)
                            Arr_LI[i].BackColor = Color.Red;
                        else if (DateTime.Now.Date < DTTmp)
                        {
                            if (DateTime.Now.AddDays(3).Date >= DTTmp)
                                Arr_LI[i].BackColor = Color.Coral;
                            else if (DateTime.Now.AddDays(7).Date >= DTTmp)
                                Arr_LI[i].BackColor = Color.Plum;
                            else
                                Arr_LI[i].BackColor = Color.LightGreen;
                        }
                    }
                    else
                    {
                        Arr_LI[i].BackColor = Color.Silver;
                        ttip.SetToolTip(Arr_LI[i], "איתור ריק");
                    }
                    PanelMain.Controls.Add(Arr_LI[i]);
                    Lbl_UpdateTo.Text = "מעודכן ל   " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                }
            }
            catch (Exception Ex)
            {   System.Diagnostics.Debug.Print(DateTime.Now + " : " + "Error - " + Ex.Message);}
        }

        private void LabelHeader(int i)
        {
            // שינוי מתאריך 25/03/2018
            // שינוי מבנה אפליקציה לבקש/ת דויד מרקוביץ מתפ"י
            try
            {
                Label LH1 = new Label();
                if (screenWidth == 1920 && screenHeight == 1080)
                {
                    LH1.Size = new Size(400, 35);
                    LH1.Font = new Font("Arial", 18, FontStyle.Bold);
                }
                else 
                {
                    LH1.Size = new Size(250, 25);
                    LH1.Font = new Font("Arial", 12, FontStyle.Bold);
                }

                LH1.TextAlign = ContentAlignment.MiddleCenter;
                LH1.BackColor = Color.LightYellow;
                LH1.Text = "Catalog             Location      QTY";

                if (i < 20)
                {
                    if (screenWidth == 1920 && screenHeight == 1080)
                        LH1.Location = new Point(110, 10);
                    else 
                        LH1.Location = new Point(110, 10);
                }
                else if (i < 40)
                {
                    if (screenWidth == 1920 && screenHeight == 1080)
                        LH1.Location = new Point(540, 10);
                    else 
                        LH1.Location = new Point(390, 10);
                }
                else if (i < 60)
                {
                    if (screenWidth == 1920 && screenHeight == 1080)
                        LH1.Location = new Point(970, 10);
                    else 
                        LH1.Location = new Point(670, 10);
                }
                else if (i < 80)
                {
                    if (screenWidth == 1920 && screenHeight == 1080)
                        LH1.Location = new Point(1400, 10);
                    else
                        LH1.Location = new Point(950, 10);
                }
                PanelMain.Controls.Add(LH1);
            }
            catch (Exception Ex)
            {   System.Diagnostics.Debug.Print(DateTime.Now + " : " + "Error - " + Ex.Message);}
        }

        private void ResizeMachiInfo()
        {
            //int screenWidth = Screen.PrimaryScreen.Bounds.Width;
            //int screenHeight = Screen.PrimaryScreen.Bounds.Height;
            //int currentDPI = 0;
            //using (Graphics g = this.CreateGraphics())
            //{
            //    currentDPI = (int)g.DpiX;
            //}
            //if (currentDPI > 96)
            //{
            //    foreach (var LBL in Arr_Lbl)
            //    {
            //        LBL.ResizeLBL(1);
            //    }
            //}
            //if (screenWidth < 1920 && screenWidth > 1600)
            //{
            //    foreach (var LBL in Arr_Lbl)
            //    {
            //        LBL.ResizeLBL(1);
            //    }
            //}
            //else if (screenWidth <= 1600 && screenWidth > 1100)
            //{
            //    foreach (var LBL in Arr_Lbl)
            //    {
            //        LBL.ResizeLBL(2);
            //    }
            //}
        }

        public void ResizeLBL(int Size)
        {
        //    if (Size == 1)
        //    {
        //        this.Height = 60;
        //        this.Width = 270;
        //        NameLBL.Height = 19;
        //        SizeLBL.Height = 19;
        //        TimeLBL.Height = 19;
        //        NameLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
        //        SizeLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
        //        TimeLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
        //    }
        //    else if (Size == 2)
        //    {
        //        this.Height = 38;
        //        this.Width = 250;
        //        NameLBL.Height = 13;
        //        SizeLBL.Height = 13;
        //        TimeLBL.Height = 13;
        //        NameLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
        //        SizeLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
        //        TimeLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
        //    }
        }


        private string FixDT(string DT)
        {
            string DTFix = "";
            try
            {                
                if (DT != "0" && DT != "")
                    DTFix = DT.Substring(6, 2) + "/" + DT.Substring(4, 2) + "/" + DT.Substring(0, 4);
                else
                    DTFix = "";
            }
            catch (Exception Ex)
            {
                System.Diagnostics.Debug.Print(DateTime.Now + " : " + "Error - " + Ex.Message);
                DTFix = "";
            }

            return DTFix;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // שינוי מתאריך 25/03/2018
            // שינוי מבנה אפליקציה לבקש/ת דויד מרקוביץ מתפ"י                 
            try
            {
                var list = (from object item in PanelMain.Controls where item is Label select item as Control).ToList();
                list.ForEach(x => PanelMain.Controls.Remove(x));
                DrawScreen();
            }
            catch (Exception Ex)
            {   System.Diagnostics.Debug.Print(DateTime.Now + " : " + "Error - " + Ex.Message);}
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 Abox = new AboutBox1();
            Abox.Show();
        }

        private void firstFloorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Floor = "FL1";
            timer1_Tick(null, null);
        }

        private void secondFloorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Floor = "FL2";
            timer1_Tick(null, null);
        }
    }
}
